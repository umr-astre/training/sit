---
title: "`sit`, R-packages and R"
subtitle: "Background information"
author: "Facundo Muñoz<br/>🖂 facundo.munoz@cirad.fr<br/>{{< fa brands twitter >}} [@famuvie](https://twitter.com/famuvie)<br /><img src=\"img/Cirad-ASTRE_Eng.png\" style=\"width: 25%\" />"
format:
  revealjs:
    theme: slide_styles.scss
    center-title-slide: false
    reveal_options:
      slideNumber: true
      progress: true

---


# R-packages {background-image="img/background_info_img - 1.png"}

::: notes
In this sequence we will briefly review some basic concepts about `sit`, `R`-packages and `R`, that are useful to know for working with `sit` or any other `R`-package.
While most of the time you don't need to know what is going on behind the scenes, these concepts are fundamental to understand the installation procedure and the messages from `R` that you will eventually have to read and interpret.

- `sit` is an __`R`-package__

	This means that it is a computer program that runs __on top of__ another computer program, which is `R`.
	In other words, it __requires__ and uses `R` to be able to work.
:::



# R-packages {background-image="img/background_info_img - 2.png"}

::: notes
- `R`

	`R` is a platform for data analysis __and__ a full _programming language_. I.e., a computer program that allows to analyse data interactively as well as write other programs using a specific language.
	Unlike other languages where you __compile__ your programs in order to be run independently from the programming language, `R` programs are __interpreted__. Users need to have `R` in their computers in order to run the program.
	`R` programs are organised and distributed as R-packages.

- `R`-packages

	`R`-packages are similar to _plugins_, _extensions_, or _add-ons_ (you might be familiar to some of those in your Internet browser). They __extend__ the functionality of the base program, leveraging its infrastructure to add new capabilities.

	`R`-packages need to be __installed__ in your computer so that they are available. Each time you use functionality from a package, you need to __load__ it.
:::



# R-packages {background-image="img/background_info_img - 3.png"}

::: notes
- `sit` also __depends__ on other `R`-packages

	`sit` also uses functionality from several other `R`-packages. For instance, package `sf` provides the infrastructure to work with geographical objects.
:::


# R-packages {background-image="img/background_info_img - 4.png"}


::: notes
This is very much like the fact that `R` needs to work on top of some Operating System, _MS Windows_, for instance, which in turns runs on top of the computer hardware!
:::


# R-packages {background-image="img/background_info_img - 5.png"}



# R-packages {background-image="img/background_info_img - 6.png"}

::: notes
- `RStudio`?

	`RStudio` is an `R` __interface__. Think of it as an _envelope_ that _wraps_ `R` and lets you interact with `R` through it, offering some convenient tools for programming and visualisation. It __requires__ the installation of `R`, but runs as a separate program.
:::


# Package versions {background-image="img/background_info_img - 6.png"}


::: notes

As any program, packages evolve incorporating features and fixing errors (bugs). We use __version numbers__ to keep track of this evolution and to identify the different stages. 

When programs depend on each other, we need to make sure to have __compatible versions__ installed for everything to work correctly. For instance, `sit` uses functionality that was introduced in `R` version 4.0. Thus, we require you to install that or a later version of `R`. Very much like you need to install the version of `R` appropriate for your Operating System, or you need an Operating System compatible with your hardware.

This fact can complicate the installation of packages, sometimes to large extents! Still, think of it as a worthy effort.
Without the huge amount of work made available by others into `R` and `R`-packages, we would have never been able to program a tool like `sit` from scratch!
:::

# Installation {background-image="img/background_info_img - 6.png"}

[CRAN]{.absolute top=240 right=-100 width=500}

![](img/background_info_arrow-2.png){.absolute top=200 right=400}

[<https://cran.r-project.org/>]{.absolute top=290 right=-100 width=500}
[<https://cloud.r-project.org/>]{.absolute top=340 right=-100 width=500}

::: notes
- CRAN

	`R`-packages can be distributed in several ways. Most `R`-packages are downloaded from the _Comprehensive R Archive Network_ (CRAN), which is the official package repository with more than 15000 `R`-packages. It has a _network_ structure with a series of servers around the globe acting as _mirrors_, keeping synchronised copies of the CRAN to deliver `R`-packages and `R` itself to nearby locations. You might be asked at some point to _choose a mirror_ to install packages from. There is a special service at https://cloud.r-project.org that chooses automatically a convenient mirror depending on your location.

:::

# Installation of `sit` {background-image="img/background_info_img - 6.png"}

[<https://umr-astre.pages.mia.inra.fr/sit>]{.absolute top=110 right=-50 width=650}

<!-- ::: {style="margin-top: 150px; font-size: 3em; color: #75AADB;"} -->
<!-- ::: {style="position: absolute; top: 105;"} -->
<!-- ```{r arrow, out.width="5em", out.height="1em"} -->
<!-- ggplot2::ggplot() + -->
<!--   ggplot2::geom_curve( -->
<!--     ggplot2::aes(x = 1, xend = 0, -->
<!--     y = 0, yend = 0), -->
<!--     curvature = 0.2, -->
<!--     arrow = ggplot2::arrow( -->
<!--       length = ggplot2::unit(0.03, "npc"), -->
<!--       type = "closed" -->
<!--     ), -->
<!--     col = "darkgray" -->
<!--   ) +  -->
<!--   ggplot2::theme_void() -->

<!-- ``` -->
<!-- ::: -->

![](img/background_info_arrow-1.png){.absolute top=105 right=620}

[CRAN]{.absolute top=240 right=-100 width=500}
![](img/background_info_arrow-2.png){.absolute top=200 right=400}
[<https://cran.r-project.org/>]{.absolute top=290 right=-100 width=500}
[<https://cloud.r-project.org/>]{.absolute top=340 right=-100 width=500}


::: notes


- `sit` is not (yet) on CRAN

	`sit` is on its way to CRAN, but for various reasons (mostly, lack of time) is not yet there. It is nonetheless online, at https://umr-astre.pages.mia.inra.fr/sit. This is a website which gives access to the code, all the documentation and tutorials and instructions for installation.

	You could also download the package and install it manually, but that would take a few more steps.
	
In summary, we started by acknowledging that `sit` is an __`R`-package__ and that it depends on other R-packages. We reviewed what a `R`-package is and, for the matter, what __`R`__ is. We highlighted that R-packages, as well as `R`, have __version numbers__ that need to be compatible and that packages are generally distributed through CRAN: the official repository of R-packages. However, `sit` needs to be installed from our website, for the moment.

:::