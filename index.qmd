---
title: "Regional Training Course on Mark-Release-Recapture and Field Data Management"
subtitle: "The International Atomic Energy Agency"
description: |
  IAEA Headquarters, Vienna. Aug. 29 - Sep. 02, 2022<br/ >
  <https://umr-astre.pages.mia.inra.fr/training/sit/>
license: "CC BY-SA"
toc: false
---


# Objectives:

1. Provide basics on MRR studies for Sterile Insect Technique (SIT) in _Aedes_
mosquitoes and the main entomological parameters to be measured to prepare pilot
trials

2. Organise and analyse the data collected from a MRR and field pilot study

3. Organise and analyse data using the R-package [`sit`](https://umr-astre.pages.mia.inra.fr/sit/)


# Materials

Please make sure to prepare in advance:

- A working laptop, with a reasonably up to date operating system, where you
have privileges to install new software.

- A recent version of [`R`](https://cran.r-project.org/) (≥ 4.1)

- A recent version of [`RStudio Desktop IDE`](https://www.rstudio.com/products/rstudio/).

  Optional, but recommended. If you are more comfortable and proficient
  with some other IDE (VSCode, Emacs+ESS, Vim, Eclipse...) you are welcome to 
  use it. Only, our capacity of help will be more limited.
  
- Try to install the [`sit`](https://umr-astre.pages.mia.inra.fr/sit/) R-package
in advance, following the instructions on the web page.

  Optional, but recommended. We will go through the installation during the
  training. But it will help if you succeeded already, or at least tried.
  
- Data sets of your own Mark-Release-Recapture studies for analysing during the
training.

  Optional, but recommended. If you don't have your own, you can use data sets
  provided with the package. Please double-check the quality of the data in
  advance as much as possible. Bring or prepare a _data dictionary_ with a
  description of the variables, their codification and the valid values or
  ranges.
  
  